import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os
from collections import defaultdict, Counter
from functools import reduce
from numpy import unique


def factorial(n):
    return reduce(lambda a, b: a * b, range(1, n + 1), 1)


def mean(x):
    return sum(x) / len(x)


def variance(x):
    return sum(map(lambda x_i: (x_i - mean(x)) ** 2, x)) / len(x)


def slicing_example():
    shape = (6, 6)
    matrix = np.zeros(shape, int)
    print(matrix.shape)
    value = 0
    for i in range(shape[0]):
        for j in range(shape[1]):
            matrix[i, j] = value + j
        value += 10
    print(matrix)
    print(matrix[4:, 4:])
    print(matrix[:, 2])
    print(matrix[2:: 2, :: 2])


def data_generation_example():
    print(factorial(5))
    x = list(range(1, 6))
    print(x)
    print(mean(x))
    print(np.mean(x))
    print(variance(x))
    print(np.var(x))
    samples = 20
    data_categorical = np.random.choice(['low', 'medium', 'high'], samples)
    data_discrete = np.random.randint(0, 10, samples)
    data_continuous = np.random.uniform(1, 10, samples)
    print(data_categorical)
    print(data_discrete)
    print(data_continuous)
    categories = unique(data_categorical)
    print(categories)
    freq_dist = defaultdict(int)
    for category in data_categorical:
        freq_dist[category] += 1
    print(freq_dist)
    freq_dist = Counter(data_discrete)
    print(freq_dist)


def convert_notebook_to_html(input_dir, output_dir):
    for file in os.listdir(input_dir):
        if file.endswith(".ipynb"):
            os.system(f'jupyter nbconvert --to html {input_dir}/{file}  --output-dir="{output_dir}"')


if __name__ == '__main__':
    # data_generation_example()
    # slicing_example()
    # convert_notebook_to_html(input_dir='./notebooks', output_dir='./slides')

    df = pd.read_csv('datasets/titanic_survivals.csv')
    df['Survived'] = df['Survived'].map({1: 'Survived', 0: 'Not Survived'})
    df.dropna(inplace=True)
    contingency_table = pd.crosstab(df.Survived, df.Sex)
    print(contingency_table)
    print(contingency_table.columns)
    contingency_table.plot.pie(y='female')
    plt.show()
    contingency_table.plot.pie(y='male')
    plt.show()
