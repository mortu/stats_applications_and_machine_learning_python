# Applicazioni statistiche e Machine Learning con Python

Introduzione a Python e Ambiente di Sviluppo (2h):

-  Installazione dell'interprete Python e PyCharm
-  Concetti base di programmazione in Python: Variabili, Costrutti Condizionale e Iterativi
-  Strutture Dati in Python: tuple, liste e dizionari

Librerie per il caricamento e visualizzazione dei dati (2h):

-  Utilizzo di Numpy per la rappresentazione numerica
-  Utilizzo di Matplotlib e Seaborn per la rappresentazione grafica avanzata
-  Utilizzo di Pandas per il caricamento dei dati

Scipy: Distribuzioni, Correlazione e Regressione (2h):

-  Introduzione alla libreria Scipy per il calcolo statistico
-  Distribuzioni: Variabili Aleatorie Continue e Discrete
-  Analisi della correlazione
-  Analisi della regressione

Scipy e statsmodels: applicazioni avanzate (4h):

-  Intervalli di confidenza
-  Test di ipotesi e ANOVA (ANalysis Of VAriance )
-  Introduzione ai Modelli Lineari: in particolare il modello di regressione lineare semplice


Sklearn: Introduzione al Machine Learning con Python (2h):

-  Introduzione a Sklearn
-  Apprendimento supervisionato: Logistic Regression,  Alberi di Decisione
-  Apprendimento non supervisionato:  K-Nearest-Neighbours, K-Means e Analisi delle Componenti Principali

## Riferimenti

### Introduction to Statistics with Python

![Cover](imgs/cover_introduction_to_statistics.jpg)


### Machine Learning Con Python

![Cover](imgs/cover_machine_learning_con_python.jpg)

### More Python Info on the Web


[http://scipy-lectures.github.com/](http://scipy-lectures.github.com/) Python Scientifc Lecture Notes. **If you read nothing else, read this!**

[https://docs.scipy.org/doc/numpy/user/numpy-for-matlab-users.html](https://docs.scipy.org/doc/numpy/user/numpy-for-matlab-users.html) Start here if you have lots of Matlab experience.

[https://docs.python.org/3.7/tutorial/](https://docs.python.org/3.6/tutorial/) The Python tutorial. The original introduction.

[http://jrjohansson.github.com/](http://jrjohansson.github.com/) Lectures on scientific computing with Python. Great ipython notebooks!
